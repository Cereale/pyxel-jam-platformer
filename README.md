# RUN:

```sh
python3 main.py
```

or 

```sh
pyxel play dist/pyxapp/pyxel-jam-platformer.pyxapp
```

# Requirements:

Pyxel 1.5 and above


![Showcase](screenshots/showcase.gif)